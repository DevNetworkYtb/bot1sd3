const Discord = require('discord.js');
const client = new Discord.Client( { ws: { intents: Discord.Intents.ALL }} );
const config = require('./config/app.json');
const Users = require('./Class/Users.js');
const Salon = require('./Class/Salon.js');
const CreateEmbed = require('./Class/CreateEmbed.js');
const fetch = require('node-fetch');
require('dotenv').config();

client.on('ready', () => {
    const guild = client.guilds.resolve(config.server.id)
    const status = [
        "La Meilleur classe",
        "Ses Développer Zachary, et Maxime",
        "Soit même"
    ]
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
    setInterval(() => {
        client.user.setActivity({type: "WATCHING", name: status[getRandomInt(3)]});
    }, 3000)
})

client.on('message', (msg) => {
    const args = msg.content.slice(config.prefix.length).trim().split(" ");
    const command = args.shift().toLowerCase();
    const guild = client.guilds.resolve(config.server.id)

    if (command === "test"){

    }

    if (command === "login") {
        function login(){
            Salon.isSalonLogin(msg.channel.id , (cb) => {
                if (cb.result === true) {
                    if (args.length === 1) {
                        if (args[0] === "eleve") {
                            let prenomIsSend = false;
                            let nomIsSend = false;
                            const message = `Veuillez entrer votre prénom :`
                            const color = config.color.messageLogin
                            const prenomEmbed = CreateEmbed.embed(message, color)
                                msg.channel.send(prenomEmbed)
                                client.on('message', (msg_prenom)=>{
                                    if (prenomIsSend === false) {
                                        Salon.isSalonLogin(msg_prenom.channel.id , (cb_two) => {
                                            if (cb_two.result === true){
                                                if (!msg_prenom.author.bot) {
                                                    const message = `Veuillez entrer votre nom :`
                                                    const color = config.color.messageLogin
                                                    const nomEmbed = CreateEmbed.embed(message, color)
                                                    msg.channel.send(nomEmbed)
                                                    const prenom = msg_prenom.content
                                                    prenomIsSend = true;
                                                    client.on('message', msg_nom => {
                                                        if (nomIsSend === false) {
                                                            Salon.isSalonLogin(msg_nom.channel.id , (cb_three) => {
                                                                if (cb_three.result === true){
                                                                    if (!msg_nom.author.bot){
                                                                        nomIsSend = true;
                                                                        const nom = msg_nom.content;
                                                                        Users.loginEleve(prenom, nom, msg_nom.author.id, cb => {
                                                                            if (cb.result === true){
                                                                                msg_nom.member.setNickname(`${prenom} ${nom[0]} .`)
                                                                                const role_eleve = guild.roles.cache.find(ro => ro.id === config.server.roles.eleve);
                                                                                msg_nom.member.roles.add(role_eleve);
                                                                                Salon.deleteSalonLogin(msg_nom.channel.id);
                                                                                msg_nom.channel.delete();
                                                                            }else {
                                                                                CreateEmbed.embedError(config.message.error.error_login.message, config.message.error.error_login.title, msg, 60000)
                                                                            }
                                                                        })
                                                                    }
                                                                } else {
                                                                    CreateEmbed.embedError(config.message.error.is_not_good_salon, "", msg, 10000)
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            }else {
                                                CreateEmbed.embedError(config.message.error.is_not_good_salon, "", msg, 10000)
                                            }
                                        })
                                    }
                                })

                        } else if (args[0] === "professeur"){
                            let nomIsSend = false;
                            let passwordIsSend = false;
                            const message = `Veuillez entrer votre nom :`
                            const color = config.color.messageLogin
                            const nomEmbed = CreateEmbed.embed(message, color)
                            msg.channel.send(nomEmbed)
                            client.on('message', (msg_nom)=>{
                                if (nomIsSend === false) {
                                    Salon.isSalonLogin(msg_nom.channel.id , (cb_two) => {
                                        if (cb_two.result === true){
                                            if (!msg_nom.author.bot) {
                                                const message = `Veuillez entrer votre mot de passe :`
                                                const color = config.color.messageLogin
                                                const passwordEmbed = CreateEmbed.embed(message, color)
                                                msg.channel.send(passwordEmbed)
                                                const nom = msg_nom.content
                                                nomIsSend = true;
                                                client.on('message', msg_password => {
                                                    if (passwordIsSend === false) {
                                                        Salon.isSalonLogin(msg_password.channel.id , (cb_three) => {
                                                            if (cb_three.result === true){
                                                                if (!msg_password.author.bot){
                                                                    passwordIsSend = true;
                                                                    const password = msg_password.content;
                                                                    Users.loginProf(nom, password, msg_password.author.id, cb => {
                                                                        if (cb.result === true){
                                                                            const role_prof = guild.roles.cache.find(ro => ro.id === config.server.roles.prof);
                                                                            msg_password.member.roles.add(role_prof);
                                                                            Salon.deleteSalonLogin(msg_password.channel.id);
                                                                            msg_password.channel.delete();
                                                                        }else {
                                                                            CreateEmbed.embedError(config.message.error.error_login.message, config.message.error.error_login.title, msg, 60000)
                                                                        }
                                                                    })
                                                                }
                                                            } else {
                                                                CreateEmbed.embedError(config.message.error.is_not_good_salon, "", msg, 10000)
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        }else {
                                            CreateEmbed.embedError(config.message.error.is_not_good_salon, "", msg, 10000)
                                        }
                                    })
                                }
                            })
                        } else {
                            const message = `l'argument ${args[0]} n'est pas dans mes fonctions !`
                            CreateEmbed.embedError(message, "", msg, 60000)
                        }
                    }else {
                        CreateEmbed.embedError(config.message.error.help_login, "", msg, 60000)
                    }
                }else {
                    CreateEmbed.embedError(config.message.error.is_not_good_salon, "", msg, 10000)
                }
            })
        }
        login()
    }

    if (command === "clear"){
        const role_perm = guild.roles.cache.find(ro => ro.id === config.server.roles.modo)
        if (msg.member.roles.cache.find(ro => ro.id === role_perm.id)){
            msg.channel.bulkDelete(100)
        }else {
            CreateEmbed.embedError(config.message.error.no_permissions, "", msg, 10000)
        }
    }

    if (command === "liste"){
        const role_perm = guild.roles.cache.find(ro => ro.id === config.server.roles.modo)
        if (msg.member.roles.cache.find(ro => ro.id === role_perm.id)){
            if (args.length <= 5){
                if (args[0] === "eleve"){
                    Users.allEleve(cb => {
                        CreateEmbed.embedListeEleve(cb.eleve, msg, 60000)
                    })
                } else if (args[0] === "search") {
                    if (args.length === 3){
                        if (args[1] === "eleve"){
                            Users.listeEleveSearch(args[2] , cb=>{
                                if (cb.result === true){
                                    CreateEmbed.embedSearchResultEleve(cb.eleve, msg, 60000)
                                }else {
                                    const message = `Nous avons point trouver ${args[2]} :(`
                                    CreateEmbed.embedError(message, "", msg, 10000)
                                }
                            })
                        }
                    }
                } else {
                    CreateEmbed.embedError(config.message.error.help_liste, "", msg, 20000)
                }
            }else {
                CreateEmbed.embedError(config.message.error.help_liste, "", msg, 20000)
            }
        }else {
            CreateEmbed.embedError(config.message.error.no_permissions, "", msg, 10000)
        }
    }
})


client.on('guildMemberAdd', (member) => {
    const guild = client.guilds.resolve(config.server.id)
    guild.channels.create(`Login ${member.user.username}`, {
        type: "text",
        parent: config.server.channels.login,
        permissionOverwrites: [{
            allow: 'VIEW_CHANNEL',
            id: member.user.id
        },
        {
            deny: 'VIEW_CHANNEL',
            id: config.server.roles.eleve
        },
        {
            deny: 'VIEW_CHANNEL',
            id: config.server.roles.prof
        }]
    }).then(result => {
        Salon.createSalonLogin(result.id).then(r => {
            const embed = new Discord.MessageEmbed()
                .setColor(config.color.messageLogin)
                .setDescription(
                    `Hey salut toi ${member} sur le serveur discord "${member.guild.name} "\n\n`+
                    `Pour continuer, et aller sur notre magnifique serveur il vous faut vous connecter \n\n`+
                    `**__Instructions__**\n`+
                    `:one: Tapez la commande : \n`+
                    '   > **-** ``!login eleve`` : pour se connecter en tant qu\'élève de la  1SD3 \n' +
                    '   > **-** ``!login professeur`` : pour se connecter en tant que professeur de la 1SD3\n' +
                    '   > **-** ``!login visiteur`` : pour se connecter en tant que visiteur sur le serveur\n\n' +
                    `:two: Lisez les instructions du Bot ! \n\n\n`+
                    `:warning: :warning: ** Quand vous mettez votre __prénom__ et __nom__ *__Veuillez mettre des majuscules avec votre nom et prénom__*, comme indiqué dans l'exemple :arrow_heading_down: ** :warning: :warning:`
                )
                .setTimestamp()

            const exemple = new Discord.MessageEmbed()
                .setColor(config.color.exempleLogin)
                .setDescription('Exemple')
                .setImage('https://nsa40.casimages.com/img/2021/02/04/210204012747455972.png')
                .setTimestamp()
            result.send(embed)
            result.send(exemple)
            result.send(`${member}`).then(r => {
                r.delete();
            })
        })
    })
})

client.login(process.env.TOKEN);