const {MessageEmbed} = require('discord.js');
const config = require('../config/app.json');

class CreateEmbed {
    static embedError(message, title, msg, timer){
        let embed;
        if (title !== "" && title !== undefined && msg && timer !== "" && msg && timer !== undefined){
            embed = new MessageEmbed()
                .setColor(config.color.error)
                .setTitle(title)
                .setDescription(message)
                .setTimestamp()
                .setFooter(
                    `${config.name}`
                )
            msg.channel.send(embed).then(ms => {
                setTimeout(() => {
                    ms.delete();
                    msg.delete();
                }, timer)
            })
        }else if(msg && timer !== "" && msg && timer !== undefined){
            embed = new MessageEmbed()
                .setColor(config.color.error)
                .setDescription(message)
                .setTimestamp()
                .setFooter(
                    `${config.name}`
                )
            msg.channel.send(embed).then(ms => {
                setTimeout(() => {
                    ms.delete();
                    msg.delete();
                }, timer)
            })
        } else {
            embed = new MessageEmbed()
                .setColor(config.color.error)
                .setDescription(message)
                .setTimestamp()
                .setFooter(
                    `${config.name}`
                )
            return embed;
        }
    }

    static embedListeEleve(user, msg, timer){
        const embed = new MessageEmbed()
            .setColor(config.color.liste_user)
            for(let i = 0; i < user.length; i++){
                Number(i);
                let connexion;
                if (user[i].connexion === 1){
                    connexion = "connecter";
                }else {
                    connexion = "deconnecter";
                }
                    embed.addField(`━━━━━━━━━━━━━━━━━━━━━━━━━━━ \n :student:  Eleve : ${user[i].nom} ${user[i].prenom}`, `**━━━━━━━━━━━━━━━━━━━━━━━━━━━** \n\n > Status : ${connexion} \n > discord : <@${user[i].id_discord}> \n > discord_id : ${user[i].id_discord} \n\n\n\n\n **☆**`)

            }
        msg.channel.send(embed).then(ms => {
            setTimeout(() => {ms.delete(); msg.delete();}, timer)
        })
    }

    static embedSearchResultEleve(user, msg, timer){
        let connexion;
        const embed = new MessageEmbed()
            .setColor(config.color.liste_user)
        if (user[0].connexion === 1){
            connexion = "connecté(e)";
        }else {
            connexion = "deconnecté(e)";
        }
        embed.addField(`━━━━━━━━━━━━━━━━━━━━━━━━━━━ \n :student:  Eleve : ${user[0].nom} ${user[0].prenom}`, `**━━━━━━━━━━━━━━━━━━━━━━━━━━━** \n\n > Status : ${connexion} \n > discord : <@${user[0].id_discord}> \n > discord_id : ${user[0].id_discord} \n\n\n\n\n **☆**`)
        msg.channel.send(embed).then(ms => {
            setTimeout(() => {ms.delete(); msg.delete();}, timer)
        })
    }

    static embed(message, color){
        let embed;
        if (message && color){
            embed = new MessageEmbed()
                .setColor(color)
                .setDescription(message)
                .setTimestamp()
                .setFooter(
                    `${config.name}`
                )
        }

        return embed;
    }
}

module.exports = CreateEmbed;