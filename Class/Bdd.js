const mysql = require('mysql');
require('dotenv').config();
class Bdd {
    static bdd(){
        const connect = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DBNAME
        })
        connect.connect((err, result, fields) => {
            if (err) throw err;
        })

        return connect;
    }
}

module.exports = Bdd;