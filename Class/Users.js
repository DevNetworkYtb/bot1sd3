const UsersModels = require('./UsersModels.js');

class Users extends UsersModels {
    static async loginEleve(prenom, nom, id_discord, result){
        await this.searchUserEleve(prenom, nom, id_discord, (cb) => {
            result({result: cb.result})
        })
    }

    static async loginProf(nom, password, user_id, result ){
        await this.searchUserProf(nom, password, user_id, (cb) => {
            result({result: cb.result})
        })
    }

    static async allEleve(cb){
        await this.searchUserAllEleve(result => {
            cb({eleve: result.eleve})
        })
    }

    static async listeEleveSearch(objet_search, cb){
        await this.searchListUserEleve(objet_search, result => {
            cb({result: result.result, eleve: result.eleve})
        })
    }
}

module.exports = Users