const Bdd = require('./Bdd.js');

class UsersModels extends Bdd{
    static searchUserEleve(prenom, nom, id_discord, cb){
        const bdd = this.bdd();
        bdd.query('SELECT * FROM liste_eleve WHERE prenom = ? AND nom = ? AND connexion = ?', [prenom, nom, 0], (err, result) => {
            if (result.length > 0){
                bdd.query('UPDATE liste_eleve SET id_discord = ?, connexion = ? WHERE id = ?', [id_discord, 1, result[0].id], (err, result) => {
                    cb({result: true})
                })
            }else {
                cb({result: false})
            }
        })
    }

    static searchUserProf(nom, password, user_id, cb) {
        const bdd = this.bdd();
        bdd.query('SELECT * FROM liste_prof WHERE name = ? AND password = ? AND connexion = ?', [nom, password, 0], (err, result) => {
            if (result.length > 0){
                bdd.query('UPDATE liste_prof SET id_discord = ?, connexion = ? WHERE id = ?', [user_id, 1, result[0].id], (err, result) => {
                    cb({result: true})
                })
            }else {
                cb({result: false})
            }
        })
    }

    static searchUserAllEleve(cb){
        const bdd = this.bdd();
        bdd.query('SELECT * FROM liste_eleve', (err, result) => {
            if (err) throw err;
            cb({eleve: result})
        })
    }

    static searchListUserEleve(objet_search, cb){
        const bdd = this.bdd();
        bdd.query('SELECT * FROM liste_eleve WHERE nom = ?',[objet_search], (err, result_by_nom) => {
            if (err) throw err;
            if (result_by_nom.length>0){
                cb({result: true, eleve: result_by_nom})
            }else {
                bdd.query('SELECT * FROM liste_eleve WHERE prenom = ?',[objet_search], (err, result_by_prenom) => {
                    if (err) throw err;
                    if (result_by_prenom.length>0){
                        cb({result: true, eleve: result_by_prenom})
                    }else {
                        bdd.query('SELECT * FROM liste_eleve WHERE id = ?',[objet_search], (err, result_by_id) => {
                            if (err) throw err;
                            if (result_by_id.length>0){
                                cb({result: true, eleve: result_by_id})
                            }else {
                                cb({result: false, eleve: null})
                            }
                        })
                    }
                })
            }
        })
    }
}

module.exports = UsersModels;