const Bdd = require('./Bdd.js');

class Salon extends Bdd{
    static async isSalonLogin(id, cb){
        const bdd = this.bdd();
        bdd.query('SELECT id_salon FROM salon_login WHERE id_salon = ?', [id], (err, result) => {
            if (err) throw err;
            if (result.length > 0){
                cb({result: true})
            }else {
                cb({result: false})
            }
        })
    }

    static async createSalonLogin(id){
        const bdd = this.bdd();
        bdd.query('INSERT INTO salon_login SET id_salon = ?', [id], (err, result) => {
            if (err) throw err;
        })
    }

    static async deleteSalonLogin(id){
        const bdd = this.bdd();
        bdd.query('DELETE FROM salon_login WHERE id_salon = ?', [id], (err, result) => {
            if (err) throw err;
        })
    }
}

module.exports = Salon;