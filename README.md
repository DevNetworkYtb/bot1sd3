## 1SD3 Bot
#

>  1SD3 bot est un bot pour sécurisé et aidez les élèves et les professeurs de la 1SD3 du lycée colbert



|Status| Commandes                          | Description                                                          | Permissions |
|---   | ---------------------------------- | -------------------------------------------------------------------  | -------- |
|🟩️    | `!login eleve`                     |Pour se connecter en tant qu'élève de la 1SD3                         |🟢        |
|🟩️    | `!login professeur`                |Pour se connecter en tant que professeur de la 1SD3                   |🟢        |
|🟥    | `!login visiteur`                  |Pour se connecter en tant que visiteur sur le serveur                 |🟢        |
|🟩️    | `!clear`                           |Pour supprimer **100** message du channel                             |🔴         |
|🟩️    | `!liste eleve`                     |Pour voir toute la liste des élèves autoriser sur le serveur !        |🔴         |
|🟥    | `!liste professeur`                |Pour voir toutes la liste des professeurs autoriser sur le serveur !  |🔴         |
|🟥    | `!liste visiteur`                  |Pour voir toutes la liste des visiteurs sur le serveur !              |🔴         |
|🟩️    | `!liste search [prénom, nom, id]`  |Pour rechercher un élève bien particulier !                           |🔴         |
|🔨     | `!liste add [prénom] [nom]`        |Pour ajouté un éleve dans la liste                                    |🔴         |


## Légende
####
### Status
>       🟩️: Disponible
>
>       🟨 : Disponible mais avec des bugs
>
>       🟥 : Indisponible
>
>       🟧 : Bientôt disponible
>
>       🔨 : en cour de développement
### Permissions 
>       🔴 : Modérateur
>
>       🟢 : Pas de permissions